#!/bin/bash

echo -e "\nInstalling System Upgrades ...\n"

# Activate automatic fan control at boot
(sudo crontab -l 2>/dev/null; echo "@reboot sudo /usr/bin/jetson_clocks") | sudo crontab -

# Go ahead and start fan now
sudo /usr/bin/jetson_clocks

# Update/upgrade/clean system
sudo apt update
sudo apt upgrade -y
sudo apt autoremove -y

# Install dependencies and productivity tools
sudo apt install terminator python-pip gfortran -y
sudo pip install --upgrade pip

echo -e "\n Completed Installing System Upgrades\n"

echo -e "\n Installing Python Dependencies ...\n"

sudo pip install pyrr numpy scipy Pillow

# Change screen lock time from default 5min to 30min
gsettings set org.gnome.desktop.session idle-delay 1800

echo -e "\n Completed Installing Python Dependencies\n"

echo -e "\nInstalling PyTorch ...\n"

cd ~
sudo apt install python-pip -y
sudo pip install --upgrade pip
wget https://nvidia.box.com/shared/static/8gcxrmcc6q4oc7xsoybk5wb26rkwugme.whl -O torch-1.2.0a0+8554416-cp27-cp27mu-linux_aarch64.whl
sudo pip install torch-1.2.0a0+8554416-cp27-cp27mu-linux_aarch64.whl
rm torch-1.2.0a0+8554416-cp27-cp27mu-linux_aarch64.whl

echo -e "\nCompleted Installing PyTorch\n"

echo -e "\nInstalling TorchVision ...\n"

sudo apt install libjpeg-dev zlib1g-dev -y
git clone -b v0.3.0 https://github.com/pytorch/vision torchvision
cd torchvision
sudo python setup.py install
cd ~

echo -e "\nCompleted Installed TorchVision\n"

echo -e "\nInstalling ROS Melodic ...\n"

sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
sudo apt-key adv --keyserver 'hkp://keyserver.ubuntu.com:80' --recv-key C1CF6E31E6BADE8868B172B4F42ED6FBAB17C654
sudo apt update
sudo apt install ros-melodic-desktop-full -y
sudo rosdep init
rosdep update
echo "source /opt/ros/melodic/setup.bash" >> ~/.bashrc
source ~/.bashrc
sudo apt install python-rosinstall python-rosinstall-generator python-wstool build-essential -y

echo -e "\nCompleted Installing ROS Melodic\n"

