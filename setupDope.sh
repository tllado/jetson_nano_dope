#!/bin/bash
 
 echo -e "\nInstalling DOPE ...\n"
 
 mkdir -p ~/catkin_ws/src
 cd ~/catkin_ws
 catkin_make
 
 cd ~/catkin_ws/src
 git clone https://github.com/NVlabs/Deep_Object_Pose.git dope
 git clone https://github.com/ros-perception/camera_info_manager_py.git
 git clone https://github.com/Kukanani/vision_msgs.git
 
 cd ~/catkin_ws/src/dope
 pip install -r requirements.txt
 
 cd ~/catkin_ws
 rosdep install --from-paths src -i --rosdistro kinetic
 sudo apt-get install ros-kinetic-rosbash ros-kinetic-ros-comm
 
 cd ~/catkin_ws
 catkin_make
 
 echo -e "\nAlmost Completed Installing DOPE\n"
 echo "Download weights from "
 echo "https://github.com/ros-perception/camera_info_manager_py"
 echo " and move them to "
 echo "~/catkin_ws/src/dope/weights"